<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use App\Repository\ArticleRepositoryInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends Controller
{
    private $articleRepository;

    public function __construct(ArticleRepositoryInterface $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    public function index()
    {
        return response(ArticleResource::collection($this->articleRepository->all()));

    }

    public function store(StoreArticleRequest $request)
    {
        return (new ArticleResource($this->articleRepository->create($request)))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show($id)
    {
        return (new ArticleResource($this->articleRepository->find($id)))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    public function update(UpdateArticleRequest $request, $id)
    {
        return $request->all();
        return (new ArticleResource($this->articleRepository->update($request, $id)))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy($id)
    {
        $this->articleRepository->delete($id);

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
