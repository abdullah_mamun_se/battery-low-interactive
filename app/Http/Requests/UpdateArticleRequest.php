<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'string',
                'required',
                'max:180',
            ],
            'slug' => [
                'string',
                'required',
                'max:180',
                Rule::unique('articles')->ignore($this->article),
            ],
            'excerpt' => [
                'string',
                'required',
                'max:300',
            ],
            'content' => [
                'string',
                'required',
            ],
            'meta_title' => [
                'string',
                'required',
                'max:180',
            ],
            'meta_description' => [
                'string',
                'required',
                'max:300',
            ],
            'meta_keyword' => [
                'string',
                'required',
                'max:300',
            ],
            'category' => [
                'string',
                'required',
                'max:255',
            ],
            'tag' => [
                'string',
                'required',
                'max:255',
            ],
            'is_featured' => [
                'boolean',
                'nullable',
            ],
            'is_school_news' => [
                'boolean',
                'nullable',
            ],
        ];
    }
}
