<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface ArticleRepositoryInterface
{
    /**
     * @param Request $request
     * @return Model
     */
    public function create(Request $request): Model;

    /**
     * @param Request $request
     * @param int $id
     * @return Model
     */
    public function update(Request $request, $id): Model;
}
