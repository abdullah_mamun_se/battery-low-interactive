<?php

namespace App\Repository\Eloquent;

use App\Models\Article;
use App\Repository\ArticleRepositoryInterface;
use App\Traits\MediaUploadingTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ArticleRepository extends BaseRepository implements ArticleRepositoryInterface
{
    use MediaUploadingTrait;

    /**
     * ArticleRepository constructor.
     *
     * @param Article $model
     */
    public function __construct(Article $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Request $request
     *
     * @return Model
     */
    public function create(Request $request): Model
    {
        $path = storage_path('tmp/uploads/');
        try {
            if (!file_exists($path)) {
                mkdir($path, 0755, true);
            }
        } catch (\Exception $e) {
        }
        
        if($request->featured_image)
        {
            $name = uniqid().'.' . explode('/', explode(':', substr($request->featured_image, 0, strpos($request->featured_image, ';')))[1])[1];
            \Image::make($request->featured_image)->save($path.$name);
            $request->merge(['featured_image' => $name]);
        }

        if($request->meta_image)
        {
            $name = uniqid().'.' . explode('/', explode(':', substr($request->meta_image, 0, strpos($request->meta_image, ';')))[1])[1];
            \Image::make($request->meta_image)->save($path.$name);
            $request->merge(['meta_image' => $name]);
        }

        $article = $this->model->create($request->all());

        return $article;
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return Model
     */
    public function update(Request $request, $id): Model
    {
        $path = storage_path('tmp/uploads/');
        try {
            if (!file_exists($path)) {
                mkdir($path, 0755, true);
            }
        } catch (\Exception $e) {
        }

        $article = $this->model->find($id);
        $c_featured_image = $article->featured_image;
        $c_meta_image = $article->meta_image;

        if($request->featured_image != $c_featured_image)
        {
            $name = uniqid().'.' . explode('/', explode(':', substr($request->featured_image, 0, strpos($request->featured_image, ';')))[1])[1];
            \Image::make($request->featured_image)->save($path.$name);
            $request->merge(['featured_image' => $name]);

            if(file_exists($path . $c_featured_image)){
                @unlink($path . $c_featured_image);
            }
        }

        if($request->meta_image != $c_meta_image)
        {
            $name = uniqid().'.' . explode('/', explode(':', substr($request->meta_image, 0, strpos($request->meta_image, ';')))[1])[1];
            \Image::make($request->meta_image)->save($path.$name);
            $request->merge(['meta_image' => $name]);

            if(file_exists($path . $c_meta_image)){
                @unlink($path . $c_meta_image);
            }
        }

        $article->update($request->all());

        return $article;
    }
}
