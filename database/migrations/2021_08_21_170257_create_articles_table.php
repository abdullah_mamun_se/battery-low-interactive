<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('title', 180);
            $table->string('slug', 180)->unique();
            $table->string('excerpt', 300);
            $table->text('content');
            $table->string('meta_title', 180);
            $table->string('meta_description', 300);
            $table->string('meta_keyword', 300);
            $table->string('category', 255)->nullable();
            $table->string('tag', 255)->nullable();
            $table->string('featured_image', 255);
            $table->string('meta_image', 255);
            $table->boolean('is_featured')->default(false);
            $table->boolean('is_school_news')->default(false);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
